import { Component, OnInit, ViewChild } from '@angular/core';
// import { MatStepper } from '@angular/material';
// import { MatStepper } from '@angular/material/stepper';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete, MatStepper} from '@angular/material';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.css']
})
export class StepperComponent implements OnInit {

  @ViewChild('stepper') stepper: MatStepper;

  constructor() { }

  ngOnInit() {
    this.stepper.selectedIndex=1;
  }

}
