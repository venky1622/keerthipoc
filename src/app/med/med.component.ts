import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, ViewChild, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete, MatStepper} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-med',
  templateUrl: './med.component.html',
  styleUrls: ['./med.component.css']
})
export class MedComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  drugCtrl = new FormControl();
  filtereddrug: Observable<string[]>;
  drug: string[] = [];
  drugs: string[] = ['Ibuprofen', 'stannous fluoride', 'Acetaminophen', 'Aluminum Zirconium', 'SODIUM FLUORIDE', 'Methocarbamol', 'ETHYL ALCOHOL'];

  @ViewChild('drugInput') drugInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild('stepper') stepper: MatStepper;

  constructor() { 

    this.filtereddrug = this.drugCtrl.valueChanges.pipe(
      map((drug: string ) => drug ? this._filter(drug) : this.drugs.slice()));

  }

  ngOnInit() { }
  add(event: MatChipInputEvent): void {
    console.log(event)
    // Add drug only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our drug
      if ((value).trim()) {
        this.drug.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.drugCtrl.setValue(null);
    }
  }

  remove(drug: string): void {
    console.log(drug);
    const index = this.drug.indexOf(drug);

    if (index >= 0) {
      this.drug.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.drug.push(event.option.viewValue);
    this.drugInput.nativeElement.value = '';
    this.drugCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    console.log(value);
    const filterValue = value.toLowerCase();

    return this.drugs.filter(drug => drug.toLowerCase().indexOf(filterValue) === 0);
  }

  fnNumbers(event) {
    // return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
    if (event.charCode < 48 || event.charCode > 57) {
      return false;
    }
  }

  fnAlphabets(event) {

    if (event.charCode != 32 && event.charCode <= 46 || (event.charCode > 46 && event.charCode < 65) || (event.charCode > 90 && event.charCode < 97) || event.charCode > 122) {
      // Restrict Numbers and dot (.)
      return false;
    }
  }
}
